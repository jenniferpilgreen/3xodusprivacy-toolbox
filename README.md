# Tool-box

### Basic ?.apk scan:
```
./baksmali list classes ?.apk | grep / | sort > ttbaksmali
(./dexdump ?.apk | grep "Class descriptor" | sed 's/ Class descriptor  : //' | grep / |sort | uniq > ttdexdump)
grep -Ff xSIGNs.txt ttbaksmali | sort --ignore-case
```


### Code_signature additions to trackers.txt (Izzy lists https://gitlab.com/IzzyOnDroid/repo/tree/master/libs )
> Izzy existing ones: 2
```
froger_mcs____com.frogermcs.activityframemetrics____http://frogermcs.github.io/FrameMetrics-realtime-app-smoothness-tracking/____nc____8888
FlowUp____io.flowup____https://github.com/flowupio/FlowUpAndroidSDK____nc____8888
```
> Izzy modified ones: 4+2
```
Parse____com.parse.Parse____https://docs.parseplatform.org/android/guide/#analytics____nc____8888
RJFUN____com.rjfun.cordova.ad____http://www.rjfun.com/w/index.php/en/____nc____8888
AWS Kinesis____com.amazonaws.metrics.____https://github.com/aws-amplify/aws-sdk-android____nc____8888
AWS Kinesis____com.amazonaws.util.AWSRequestMetrics.____https://github.com/aws-amplify/aws-sdk-android____nc____8888

BugSense____com.bugsense.trace.____https://github.com/bugsense/docs/blob/master/android.md____nc____8888
?DebugDrawer____io.palaima.debugdrawer.____https://github.com/palaima/DebugDrawer____nc____9999
```
> ETIP stand-by: 1 known
```
²GravyAnalytics____com.timerazor.gravysdk.____http://gravyanalytics.com____api.findgravy.com|ws.findgravy.com____8888
```
> Missing ones: 18 known + 7 possible
```
°BugClipper____com.bugclipper.android.____https://bugclipper.com/sdk-doc/android/____nc____8888
°?Buglife____com.buglife.sdk____https://buglife.com/docs/android____nc____9999
°?Flipper____com.facebook.flipper.____https://github.com/facebook/flipper/search?q=com.facebook.flipper and unscoped_q=com.facebook.flipper____nc____9999
°?Rollbar____com.rollbar.android.____https://docs.rollbar.com/docs/android____nc____9999
°?Critic____io.inventiv.critic.____https://github.com/inventivtools/inventiv-critic-android____nc____9999
°?Raygun____com.mindscapehq.android.raygun4android____https://raygun.com/documentation/language-guides/android/installation/____nc____9999
°?Game Sparks____com.gamesparks.sdk.____https://docs.gamesparks.com/sdk-center/android.html____nc____9999
°?PlayFab java____com.playfab.PlayFab____https://api.playfab.com/docs/getting-started/java-getting-started____nc____9999
°Skillz____com.skillz.Skillz____https://skillz.zendesk.com/hc/en-us/articles/208579286-Android-Core-Implementation____nc____8888
°Woopra____com.woopra.tracking.____https://github.com/Woopra/woopra-android-sdk____nc____9999
°Askingpoint____com.askingpoint.____https://www.askingpoint.com/blog/document/add-sdk-to-project/____nc____8888
°Databox____com.databox.____https://developers.databox.com/sdks/  and  https://github.com/databox/databox-java____nc____9999
°Teliver____com.teliver.sdk____https://www.teliver.io/____nc____8888
°AppLink.io____io.applink.applinkio.AppLinkIO____https://support.applink.io/hc/en-us/articles/360021172012-Getting-Started-with-the-React-Native-SDK____nc____8888
°mTraction____com.mtraction.mtractioninapptracker.____http://docs.mtraction.com/index.php/docs/integrations/android-unity-integration/____nc____8888
°Pulsate____com.pulsatehq.____https://pulsate.readme.io/v2.0/docs/installing-sdk-android-studio____nc____8888
°SAS____com.sas.mkt.mobile.sdk.____https://communities.sas.com/t5/SAS-Communities-Library/Building-a-SAS-CI-enabled-mobile-app-for-Android/ta-p/354922____nc____8888
°Backendless____com.backendless.____https://backendless.com/docs/android/quick_start_guide.html____nc____8888

µ?Factual____com.factual.engine.____https://developer.factual.com/docs/engine-sdk-android-guide____nc____8888
µ?Factual____com.factual.Factual____https://developer.factual.com/docs/engine-sdk-android-guide____nc____8888
µ?Custom Activity On Crash____cat.ereza.customactivityoncrash____https://github.com/Ereza/CustomActivityOnCrash____nc____9999
µ?ACRA____org.acra.____https://github.com/ACRA/acra____nc____9999
µ?Acrarium____com.faendir.acra.____https://github.com/F43nd1r/Acrarium____nc____9999
µ?CleanInsights____io.cleaninsights.sdk.piwik.____https://github.com/cleaninsights/cleaninsights-android-sdk/blob/master/cleaninsights-piwik-sdk/src/main/java/io/cleaninsights/sdk/CleanInsights.java____nc____9999
µ?CleanInsights____io.cleaninsights.sdk.CleanInsights.____https://github.com/cleaninsights/cleaninsights-android-sdk/blob/master/cleaninsights-piwik-sdk/src/main/java/io/cleaninsights/sdk/CleanInsights.java____nc____9999
```
> Exodus misc. : 3
```
Demdex____com.adobe.mobile.Config____https://www.adobe.com/data-analytics-cloud/audience-manager.html____demdex\.net____40
Instabug____com.instabug.library.Instabug____https://instabug.com/crash-reporting____NC____206
S4M____.S4MAnalytic____http://www.s4m.io/____sam4m\.com|s4m.io____20
```


#### Basic uses
```
cat trackers312NEW.txt | sort --ignore-case | awk -F "____" {'print $1"~"$5'} | uniq > xList.txt
cat trackers312NEW.txt | awk -F "____" {'print $2'} | sed -e "s/\./\//g"| sort --ignore-case  > xSIGNs.txt
```

#### Izzy X libs: https://gitlab.com/IzzyOnDroid/repo/tree/master/libs
```
cat xSIGNs.txt | sed -r 's/\/$//' | xargs -I {} grep {} libinfo.txt
cat xSIGNs.txt | sed -r 's/\/$//' | xargs -I {} grep {} libsmali.txt
(tr A-Z a-z < xSIGNs.txt | sed -r 's/\/$//' | xargs -I {} grep {} libsmali.txt | wc -l)
```


###### Simplified stat (indexExodus eof:312)
330 IDs(xList) = 398 signatures(xSIGNs)